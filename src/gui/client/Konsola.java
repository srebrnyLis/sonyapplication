package gui.client;

import javafx.beans.property.SimpleStringProperty;

public class Konsola {

	private final SimpleStringProperty idProduktu;
	private final SimpleStringProperty model;
	private final SimpleStringProperty procesor;
	private final SimpleStringProperty pamiecOperacyjna;
	private final SimpleStringProperty dyskTwardy;
	private final SimpleStringProperty procesorGraficzny;
	private final SimpleStringProperty data;
	
	public Konsola(String id, String model, String procesor, String pamiec, String dysk, String procesorG, String data){
		this.idProduktu= new SimpleStringProperty(id);
		this.model= new SimpleStringProperty(model);
		this.procesor= new SimpleStringProperty(procesor);
		this.pamiecOperacyjna= new SimpleStringProperty(pamiec);
		this.dyskTwardy= new SimpleStringProperty(dysk);
		this.procesorGraficzny= new SimpleStringProperty(procesorG);
		this.data= new SimpleStringProperty(data);
		
	}

	public SimpleStringProperty idProduktuProperty(){
		return this.idProduktu;
	}
	
	public String getidProduktu(){
		return this.idProduktuProperty().get();
	}
	
	public void setIdProduktu(final String idProduktu){
		this.idProduktuProperty().set(idProduktu);
	}
		
	
}
