package clientServerConnection;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.Connection;
import java.sql.Statement;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

import database.server.Database;




public class ServerTCPcall implements Callable<String> {

	Socket mySocket;
	FutureTaskCallback<String> ft;
	DataInputStream input;
	DataOutputStream output;
	String polecenie;
	
	int id_pojedynczego_rekordu;
	int result = 0;

	public ServerTCPcall(Socket socket) {
		// System.out.println("SerwerTCPcall - odbieram od klienta");
		mySocket = socket;
		ft = new FutureTaskCallback<String>(this);
	}

	public FutureTaskCallback<String> getFt() {
		return ft;
	}

	@Override
	public String call() throws Exception {
		String txt = mySocket.getInetAddress().getHostName();
		try {
			input = new DataInputStream(mySocket.getInputStream());
			output = new DataOutputStream(mySocket.getOutputStream());

			polecenie = input.readUTF();
			System.out.println("Odebralem od klienta polecenie: " + polecenie);
			System.out.println("Wywoluje metode ObsluzPolecenieSQL");
			ObsluzPolecenieSQL(polecenie);
			
			/*
			 * Dodamy statyczna zmienna
			 * bedziemy do niej przypisywac id polecenia
			 * bedziemy wyswietlac na serwerze informacje - Wykonano np. logowanie
			 * w zaleznosci od tego jaki numer polecenia zostanie wykonany (dodatkowa metoda
			 * ze switchem)
			 * 
			 */
			
			
			

		} catch (Exception e) {
			System.err.println(e);
		}
		return "Done. Socket " + txt + " is closed.";
	}
	
	
	
	
	
	
	

	public void ObsluzPolecenieSQL(String polecenie) {
		Database bd = new Database();
		Connection con = bd.connectToDatabase();
		Statement st = bd.createStatement(con);

		String[] tab = polecenie.split(" ");
		int liczba = Integer.parseInt(tab[0]);
		if (liczba == 1 || /*liczba == 2 ||*/ liczba == 4 || liczba == 8 ) {
			//WYKONANIE ZWYKLEGO SELECTA I ZRZUTOWANIE WYNIKU STRING NA INT ZEBY MOZNA BYLO UZYC writeInt()
			//id_pojedynczego_rekordu = Integer.parseInt(bd.wykonaniePolecenia(con, polecenie));
			String tmp = bd.wykonaniePolecenia(con, polecenie);
			String []tmpArray = tmp.split(" ");
			id_pojedynczego_rekordu=Integer.parseInt(tmpArray[0]);
			System.out.println("Wysylam do klienta id rekordu: " + id_pojedynczego_rekordu);
			try {
				//output = new DataOutputStream(mySocket.getOutputStream());
				output.writeInt(id_pojedynczego_rekordu);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("\n>> Blad - output.writeInt(id_pojedynczego_rekordu) <<\n");
				e.printStackTrace();
			}
		} else if(liczba==7 || liczba==15){ 
			String tmp = bd.wykonaniePolecenia(con, polecenie);
			System.out.println("Wysylam do klienta id rekordu: " + id_pojedynczego_rekordu);
			try {
				//output = new DataOutputStream(mySocket.getOutputStream());
				output.writeUTF(tmp);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("\n>> Blad - output.writeUTF(tmp) <<\n");
				e.printStackTrace();
			}
		
		}else{
			
			result = bd.wykonaniePolecenia2(con, polecenie);
			//String tmp=""+result;
			System.out.println("Wysylam do klienta rekordy: " + result);
			try {
				//output = new DataOutputStream(mySocket.getOutputStream());
				output.writeInt(result);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("\n>> Blad - output.writeUTF(result) <<\n");
				e.printStackTrace();
			}
		}

		Database.closeConnection(con, st);
	}
}