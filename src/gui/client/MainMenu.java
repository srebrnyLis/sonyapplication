package gui.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import Safety.Asking;
import clientServerConnection.Server;



public class MainMenu {
	private Stage stage;
	private Scene sceneLogOut;
	private Scene scene;

	private Button addButton;
	private Button searchButton;
	private Button reservButton;
	private Button accountButton;
	private Button travelButton;
	private Button chatButton;
	private Button logOutButton;
	private Button settingsButton;
	
	
	public int userId;

	// ============================================================
	public MainMenu(Stage stage, Scene sceneWyloguj, int userId) {
		this.stage = stage;
		this.stage.setTitle("Menu");
		this.stage.setMinWidth(400);
		this.stage.setMinHeight(500);
		this.stage.setMaxWidth(400);
		this.stage.setMaxHeight(500);
		this.sceneLogOut = sceneWyloguj;
		this.userId = userId;
		initButtons();
		initlayouts();
	}

	// ============================================================
	private void initlayouts() {
		GridPane grid = new GridPane();
		grid.setPadding(new Insets(10, 10, 10, 10));// A set of inside offsets
													// for the 4 side of a
													// rectangular area
		grid.setVgap(8);
		grid.setHgap(10);
		// grid.setAlignment(Pos.CENTER);

		grid.getChildren().addAll(addButton, searchButton, reservButton, travelButton, accountButton,
				logOutButton);
		grid.setAlignment(Pos.CENTER);
		scene = new Scene(grid, 400, 400);
		stage.setScene(scene);
		stage.setTitle("Menu");

		stage.show();
	}

	// ============================================================
	private void initButtons() {
		// ============================================================
		// Add travel Button
		addButton = new Button("Wyswietl produkty");
		addButton.setPrefSize(150, 50);
		GridPane.setConstraints(addButton, 0, 1);
		addButton.setOnAction(e -> {
			System.out.println("Klikam wyswietl produkty");
			new Produkty(stage,sceneLogOut,scene,userId);
			//travel = new Dodaj_przejazd(stage, sceneWyloguj, scene, userId);
		});
		// ============================================================
		// Search travel Button
		searchButton = new Button("Porownaj produkty");
		searchButton.setPrefSize(150, 50);
		GridPane.setConstraints(searchButton, 0, 2);
		searchButton.setOnAction(e -> {
			System.out.println("Klikam porownaj produkty");
			//search = new Wyszukaj_przejazd(stage, sceneWyloguj, scene, userId);
		});
		// ============================================================
		// Reservation Button
		reservButton = new Button("Ostatnio przegladane");
		reservButton.setPrefSize(150, 50);
		GridPane.setConstraints(reservButton, 0, 3);
		reservButton.setOnAction(e -> {
			System.out.println("Klikam ostatnio przegladane");
			//reserve = new Moje_Rezerwacje(stage, sceneWyloguj, scene, userId);
		});
		// ============================================================
		// Travels Button
		travelButton = new Button("Wyswietl najnowsze");
		travelButton.setPrefSize(150, 50);
		GridPane.setConstraints(travelButton, 0, 4);
		travelButton.setOnAction(e -> {
			System.out.println("Klikam wyswietl najnowsze");
			//myTravel =new Moje_przejazdy(stage, sceneWyloguj, scene, userId);
		});

		// Settings button
		settingsButton = new Button("Ustawienia");
		settingsButton.setPrefSize(150, 50);
		GridPane.setConstraints(settingsButton, 0, 5);
		settingsButton.setOnAction(e -> {
			System.out.println("Klikam ustawienia");
			// myTravel =new Moje_przejazdy(stage, sceneWyloguj, scene, userId);
		});
		
		// ============================================================
		// Account Button
		accountButton = new Button("Moje Konto");
		accountButton.setPrefSize(150, 50);
		GridPane.setConstraints(accountButton, 0, 6);
		accountButton.setOnAction(e -> {
			System.out.println("\nKlikam moje konto. Serwer.loggedUserId = " + Server.loggedUserId);
			new Moje_konto(stage, sceneLogOut, scene, userId);
			//account = new MyAccount(stage,sceneLogOut,scene, Server.loggedUserId);
		});


		// Log Out Button
		logOutButton = new Button("Wyloguj");
		logOutButton.setPrefSize(150, 50);
		GridPane.setConstraints(logOutButton, 0, 7);		
		logOutButton.setOnAction(e -> {
			boolean result = Asking.display("Wylogowywanie", "Czy na pewno chcesz sie wylogowac?");
			if (result == true) {
				Server.loggedUserId = 0;		//to nie dziala
				System.out.println("\nWylogowano\n");
				stage.setScene(sceneLogOut);
			}
		});

	}
}