package Safety;


import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class Asking {
	
	static boolean answer;
	
	public static boolean display(String title, String massage){
		Stage window = new Stage();
//============================================================	
		window.initModality(Modality.APPLICATION_MODAL);//Blokuje interakcje z  innymi blokami
		window.setTitle(title);
		window.setMinWidth(250);//zatrzymuje przed zmniejszaniem do tej wartosci podanej
		window.setMinHeight(250);
//============================================================		
		Label label = new Label();
		label.setText(massage);
//============================================================	
		Button yesButton = new Button("Tak");
		Button noButton = new Button ("Nie");
//============================================================	
		yesButton.setOnAction(e -> {
			answer = true;
											//otworzneie okna New Account !!!! doddac do lambda
			window.close();
		});
//============================================================		
		noButton.setOnAction(e -> {
			answer = false;
			window.close();
		});
//============================================================	
		GridPane layout = new GridPane();
		layout.setPadding(new Insets(2,2,2,2));
		layout.setVgap(2);
		layout.setHgap(2);
		layout.setConstraints(label, 0, 0);
		layout.setConstraints(yesButton, 0, 1);
		layout.setConstraints(noButton, 1, 1);
		layout.getChildren().addAll(label,noButton,yesButton);
		layout.setAlignment(Pos.CENTER);
		
		Scene scene = new Scene(layout);
		window.setScene(scene);
		window.showAndWait();
		
		return answer ;
	}

}
