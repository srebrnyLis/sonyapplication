package clientServerConnection;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class Server extends Application implements Runnable {
	// GUI SERWERA:
	Stage stage;
	Scene scene1;
	private static TextArea label;

	public static int loggedUserId;

		
	
	
	// ==============================================================================================
	public static void main(String args[]) {
		new Thread(new Server()).start();
		launch(args);
	}

	// ==============================================================================================

	
	
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		int port = 1234;
		ServerSocket serverSocket = null;
		try {
			serverSocket = new ServerSocket(port);
			ExecutorService exec = Executors.newCachedThreadPool();
			while (true) {
				Socket socket = serverSocket.accept();
				exec.execute(new ServerTCPcall(socket).getFt());
			}

		} catch (Exception e) {
			System.out.println("Server b��d: " + e.getMessage());
		} finally {
			if (serverSocket != null)
				try {
					serverSocket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		stage = primaryStage;
		stage.setTitle("Serwer");
		stage.setMinHeight(600);
		stage.setMinHeight(600);
		// ==============================================
		initLabel();
		initLayouts();
	}

	private void initLayouts() {
		BorderPane border = new BorderPane();

		border.setCenter(label);

		scene1 = new Scene(border, 600, 600);
		stage.setScene(scene1);
		stage.show();
	}

	private void initLabel() {
		label = new TextArea();
		label.setPrefSize(410, 306);
		label.setEditable(false);
	}

}