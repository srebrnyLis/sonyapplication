package clientServerConnection;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;


import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import Safety.Alert;
import Safety.Asking;
import Safety.Author;
import database.client.LogIn;
import gui.client.*;
//import Scene.Rejestracja;

public class Client extends Application implements Runnable {
	Stage stage;
	Scene scene1, scene2;

	private Label nameLabel;
	private Label passLabel;
	private TextField nameInput;
	private PasswordField passInput;
	private Button loginButton;
	private Button signButton;
	private Button authorButton;

	private Rejestracja register;
	private MainMenu mainMenu;

	public int userId = 0;
	public String str = "";

	public DataInputStream input;
	public DataOutputStream output;

	Socket s;
	int port = 1234;
	String nazwa = "127.0.0.1";

	public void run() {
		try {
			s = new Socket(InetAddress.getByName(nazwa), port);
			output = new DataOutputStream(s.getOutputStream());
			input = new DataInputStream(s.getInputStream());

			while (true) {
				try {
					// System.out.println("\nOdbieram w p�tli od serwera");
					// String zmienna = input.readUTF();
					// System.out.println("\ninput = " + zmienna);
				} catch (Exception e) {
					System.out.println("\n>> B��d pobrania danych <<");
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		new Thread(new Client()).start();
		launch(args);

		System.out.println("\nKlient stop");
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// Logowanie_okno okno = new Logowanie_okno();\

		stage = primaryStage;
		stage.setTitle("CarPooling");
		stage.setMinWidth(400);// zatrzymuje przed zmniejszaniem do tej wartosci
								// podanej
		stage.setMinHeight(400);
		// ============================================================

		// Do zamykania aplikacji
		stage.setOnCloseRequest(e -> {
			e.consume(); // is used to eliminate a keypad(keyboard) operation
							// after a certain opereation
			closeProgram(); // wywolywanie metody
		});

		initLabel();
		initTextField();
		initButtons();
		initLayouts();
	}

	// ============================================================
	private void initLayouts() {
		GridPane grid = new GridPane();
		grid.setPadding(new Insets(10, 10, 10, 10));
		grid.setVgap(8);
		grid.setHgap(10);
		// ============================================================
		// Dodawanie wszystkich komponentow
		grid.getChildren().addAll(nameLabel, nameInput, passLabel, passInput, loginButton, signButton, authorButton);
		scene1 = new Scene(grid, 400, 400);
		stage.setScene(scene1);
		stage.setTitle("Login");
		stage.show();

	}

	// ============================================================
	private void initLabel() {
		// Name Label
		nameLabel = new Label("Nazwa uzytkownika: ");
		GridPane.setConstraints(nameLabel, 0, 0);
		// ============================================================
		// Password label
		passLabel = new Label("Haslo: ");
		GridPane.setConstraints(passLabel, 0, 1);
	}

	// ============================================================
	private void initTextField() {
		// Name input
		nameInput = new TextField();
		nameInput.setPromptText("Pseudonim");
		GridPane.setConstraints(nameInput, 1, 0);
		// ============================================================
		// Password input
		passInput = new PasswordField();
		passInput.setPromptText("Haslo");
		GridPane.setConstraints(passInput, 1, 1);

	}

	// ============================================================
	private void initButtons() {
		// Button Login
		loginButton = new Button("Zaloguj");
		GridPane.setConstraints(loginButton, 1, 2);
		loginButton.setOnAction(e -> {
			System.out.println("userId przed zalogowaniem = " + Server.loggedUserId);
			System.out.println("Klikam zaloguj");

			LogIn logIn = new LogIn(nameInput.getText(), passInput.getText(), userId);
					
			if (!logIn.Log()) {
				Alert.display("Error", "Nie istnieje taki uzytkownik lub podales bledne dane");
			} else {		
				userId = logIn.getUserId();
				System.out.println("UserId po zalogowaniu: " + userId);
				mainMenu = new MainMenu(stage, scene1, userId);
			}

		});

		// ============================================================
		// Button Sing up
		signButton = new Button("Zaloz konto");
		GridPane.setConstraints(signButton, 1, 3);
		signButton.setOnAction(e -> {
			boolean result = Asking.display("Nowe konto", "Stworz nowe konto?");
			if (result == true) {
				System.out.println("Klikam rejestracja");
				register = new Rejestracja(stage, scene1);
			}
		});
		// ============================================================
		authorButton = new Button("Autorzy");
		GridPane.setConstraints(authorButton, 1, 4);
		authorButton.setOnAction(e -> {
			Author.display("Autorzy", null);
		});
	}

	// =================================================================
	// Zabezpieczenie do zamykania okien programu || zadaje zapytanie czy wyjsc
	private void closeProgram() {
		Boolean answer = Asking.display("Wyjscie", "Czy na pewno chcesz wyjsc?");
		if (answer == true) {
			stage.close();
		}
	}
}