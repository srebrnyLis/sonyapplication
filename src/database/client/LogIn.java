package database.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

public class LogIn {
	public DataInputStream input;
	public DataOutputStream output;
	Socket s;
	int port = 1234;
	String nazwa = "127.0.0.1";

	public int userId = 0;
	
	public int getUserId() {
		return userId;
	}

	public String str = "";

	private String name;
	private String password;

	public LogIn(String name, String password, int userId) {
		this.name = name;
		this.password = password;
		this.userId = userId;
	}

	// Metoda loguje do aplikacji
	public boolean Log() {
		try {
			s = new Socket(InetAddress.getByName(nazwa), port);
			output = new DataOutputStream(s.getOutputStream());
			input = new DataInputStream(s.getInputStream());

			str = "1 " + name + " " + password;

			try {
				System.out.println("Wysylam polecenie Zaloguj() na serwer");
				output.writeUTF(str);
			} catch (IOException e1) {
				System.out.println("\n>> Blad wysylania polecenia na serwer <<\n");
			}

			System.out.println("Proba odbioru od serwera");
			try {
				userId = input.readInt();
				//System.out.println("Odebrano od serwera id zalogowanego uzytkownika. logIn.userId = " + userId);
			} catch (IOException e2) {
				System.out.println("\n>> Blad odbioru od serwera <<\n");
			}

			if (userId != 0)
				return true;

		} catch (IOException e3) {
			// TODO Auto-generated catch block
			System.out.println("IOException e3 - Zaloguj");
			e3.printStackTrace();
		}

		System.out.println("\n>> userId jest rowne 0. Nie zalogowano! <<\n");
		return false;
	}
}