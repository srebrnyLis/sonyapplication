package gui.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

import Safety.Asking;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class WyswietlProdukt {

	private Stage stage;
	private Scene sceneLogOut;
	private Scene scene;

	private Button addButton;
	private Button searchButton;
	private Button reservButton;
	private Button accountButton;
	private Button travelButton;
	private Button chatButton;
	private Button logOutButton;
	private Button settingsButton;
	private Button backButton;
	//private RadioButton radioButton;
	private Label menuLabel;
	private VBox gridProducts;
	int userId;
	
	private RadioButton radioButton1;
	private RadioButton radioButton2;
	boolean isProduct;
	
	private TableView<String> table;
	public DataInputStream input;
	public DataOutputStream output;
	Socket s;
	int port = 1234;
	String nazwa = "127.0.0.1";
	String tabela="";
	String []tablica;
	
	public WyswietlProdukt(Stage stage, Scene sceneWyloguj, Scene scene, int userId, boolean isProduct, String tabela) {
		this.stage = stage;
		this.stage.setTitle("Produkty");
		this.stage.setMinWidth(400);
		this.stage.setMinHeight(500);
		this.stage.setMaxWidth(800);
		this.stage.setMaxHeight(1000);
		this.sceneLogOut = sceneWyloguj;
		this.userId = userId;
		this.isProduct = isProduct;
		this.tabela=tabela;
		
			tablica=tabela.split(" ");
		initLayouts();
		gridProducts.getChildren().addAll(createGridProducts());
	}
	
	private void initLayouts() {

		GridPane gridMenu = new GridPane();
		//GridPane gridAccount = new GridPane();
		//GridPane gridButton = new GridPane();

		gridMenu = createGridMenu();
		//gridTravel = new VBox();
		//gridAccount = createGridAccount();
		//gridButton = createGridButton();
		//gridTravel = createGridTravel();

		//GridPane gridMenu = new GridPane();	
		gridMenu = createGridMenu();
		gridProducts = new VBox();

		BorderPane borderPane = new BorderPane();
		borderPane.setLeft(gridMenu);
		
		//gridProducts = createGridProducts();

		borderPane.setCenter(gridProducts);
		
		//czy wyswietlamy radiobutton
		if(isProduct){
			borderPane.setRight(createGridRadio());
		}
		else{
			wyswietl(tablica[0]);
		}
		
		Scene scene = new Scene(borderPane, 600, 400);
		stage.setScene(scene);
		stage.setTitle("Produkty");
		stage.show();
		

	}
	
	private void initButtons_2() {

		// Chat Button
		//chatButton = new Button("Otworz Czat");
		//chatButton.setPrefSize(150, 50);
		//GridPane.setConstraints(chatButton, 0, 1);
		//chatButton.setOnAction(e -> {
			//chat = new Czat(stage, sceneLogout, sceneBack, Klient.id);
		//});

		// Add travel Button
		addButton = new Button("Dodaj Przejazd");
		addButton.setPrefSize(150, 50);
		GridPane.setConstraints(addButton, 0, 2);
		addButton.setOnAction(e -> {
			//travel = new Dodaj_przejazd(stage, sceneLogout, sceneBack, Klient.id);
		});

		// Search travel Button
		searchButton = new Button("Wyszukaj Przejazd");
		searchButton.setPrefSize(150, 50);
		GridPane.setConstraints(searchButton, 0, 3);
		searchButton.setOnAction(e -> {
			//search = new Wyszukaj_przejazd(stage, sceneLogout, sceneBack, Klient.id);
		});

		// Reservation Button
		reservButton = new Button("Moje Rezerwacje");
		reservButton.setPrefSize(150, 50);
		GridPane.setConstraints(reservButton, 0, 4);
		//reservButton.setOnAction(e -> reserve = new Moje_Rezerwacje(stage, sceneLogout, sceneBack, Klient.id));

		// Travels Button
		travelButton = new Button("Moje Przejazdy");
		travelButton.setPrefSize(150, 50);
		GridPane.setConstraints(travelButton, 0, 5);
		//travelButton.setOnAction(e -> myTravel = new Moje_przejazdy(stage, sceneLogout, sceneBack, Klient.id));

		// Account Button
		accountButton = new Button("Moje Konto");
		accountButton.setPrefSize(150, 50);
		GridPane.setConstraints(accountButton, 0, 6);
		//accountButton.setOnAction(e -> account = new Moje_konto(stage, sceneLogout, sceneBack, Klient.id));

		// Log Out Button
		logOutButton = new Button("Wyloguj");
		logOutButton.setPrefSize(150, 50);
		GridPane.setConstraints(logOutButton, 0, 8);
		logOutButton.setOnAction(e -> {
			boolean result = Asking.display("Wylogowywanie", "Czy na pewno chcesz sie wylogowac?");
			if (result == true) {
				stage.setScene(sceneLogOut);
			}
		});

		// Back Button
		backButton = new Button("Cofnij");
		backButton.setPrefSize(150, 50);
		GridPane.setConstraints(backButton, 0, 7);
		backButton.setOnAction(e -> {
			boolean result = Asking.display("Powr�t", "Czy na pewno chcesz sie cofn��?");
			if (result == true) {
				  new MainMenu(stage,sceneLogOut,userId);
			}
		});
	}
// ============================================================
	private void initLabel_2() {

		// Menu Label
		menuLabel = new Label("Menu");
		menuLabel.setPrefSize(150, 50);
		GridPane.setConstraints(menuLabel, 0, 0);
	}
// ============================================================
	public GridPane createGridMenu() {
		GridPane grid = new GridPane();
		grid.setPadding(new Insets(10, 10, 10, 10));
		grid.setVgap(10);
		grid.setHgap(10);
		grid.setAlignment(Pos.CENTER);
	// ============================================================
		initButtons_2();
		initLabel_2();
	// ============================================================
		grid.getChildren().addAll(menuLabel/*,chatButton,addButton,searchButton,reservButton,travelButton*/,accountButton,logOutButton,backButton);
		return grid;
	}
	
	

	public GridPane createGridRadio() {
		GridPane grid = new GridPane();
		grid.setPadding(new Insets(10, 10, 10, 10));
		grid.setVgap(10);
		grid.setHgap(10);
		grid.setAlignment(Pos.CENTER);
	// ============================================================
		radioButton1 = new RadioButton(tablica[0]);
		radioButton2 = new RadioButton(tablica[1]);		
		radioButton1.setPrefSize(150, 50);
		radioButton2.setPrefSize(150, 50);
		grid.setConstraints(radioButton1, 0, 0);
		grid.setConstraints(radioButton2, 0, 1);
		
		//OBSLUGA RADIOBUTTONOW
		radioButton1.setOnAction(e -> {
			//jesli odznaczony zaznaczony to zaznaczamy i wyswietlamy wyniki
			if(radioButton2.isSelected()){
				radioButton1.setSelected(true);
				radioButton2.setSelected(false);
				wyswietl(tablica[0]);
				
				
			} else {
				radioButton1.setSelected(true);
				wyswietl(tablica[1]);
			}
			
			
			
		});
		
		radioButton2.setOnAction(e -> {
			//jesli odznaczony zaznaczony to zaznaczamy i wyswietlamy wyniki
			if(radioButton1.isSelected()){
				radioButton2.setSelected(true);
				radioButton1.setSelected(false);
				
				
				
			} else {
				radioButton2.setSelected(true);				
			}
		});
		
	// ============================================================
		grid.getChildren().addAll(radioButton1, radioButton2);
		return grid;
	}
	
	
	
	
	
	public TableView createGridProducts() {

		// Id column
		TableColumn<String, String> idColumn = new TableColumn("IdUzytkownika");
		idColumn.setMinWidth(30);
		idColumn.setMaxWidth(30);
		idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));
/*
		// From column
		TableColumn<PrzejazdWyszukiwanie, String> fromColumn = new TableColumn("Miejsce poczatkowe");
		fromColumn.setMinWidth(120);
		fromColumn.setMaxWidth(120);
		fromColumn.setCellValueFactory(new PropertyValueFactory<>("zOdjazd"));

		// Nation From column
		TableColumn<PrzejazdWyszukiwanie, String> nationFromColumn = new TableColumn("Panstwo poczatkowe");
		nationFromColumn.setMinWidth(120);
		nationFromColumn.setMaxWidth(120);
		nationFromColumn.setCellValueFactory(new PropertyValueFactory<>("panstwoOdjazd"));// zmienic

		// To column
		TableColumn<PrzejazdWyszukiwanie, String> toColumn = new TableColumn("Miejsce docelowe");
		toColumn.setMinWidth(120);
		toColumn.setMaxWidth(120);
		toColumn.setCellValueFactory(new PropertyValueFactory<>("doDojazd"));

		// Nation To column
		TableColumn<PrzejazdWyszukiwanie, String> nationToColumn = new TableColumn("Panstwo docelowe");
		nationToColumn.setMinWidth(120);
		nationToColumn.setMaxWidth(120);
		nationToColumn.setCellValueFactory(new PropertyValueFactory<>("panstwoDojazd"));// zmienic

		// Date column
		TableColumn<PrzejazdWyszukiwanie, String> dateColumn = new TableColumn("Data");
		dateColumn.setMinWidth(80);
		dateColumn.setMaxWidth(80);
		dateColumn.setCellValueFactory(new PropertyValueFactory<>("data"));// zmienic

		// Date column
		TableColumn<PrzejazdWyszukiwanie, String> timeColumn = new TableColumn("Czas");
		timeColumn.setMinWidth(80);
		timeColumn.setMaxWidth(80);
		timeColumn.setCellValueFactory(new PropertyValueFactory<>("czas"));// zmienic

		// FreeSpace column
		TableColumn<PrzejazdWyszukiwanie, String> spaceColumn = new TableColumn("Wolne Miejsca");
		spaceColumn.setMinWidth(80);
		spaceColumn.setMaxWidth(80);
		spaceColumn.setCellValueFactory(new PropertyValueFactory<>("wolneMiejsca"));// zmienic

		// IDKierowcy column
		TableColumn<PrzejazdWyszukiwanie, String> driverIdColumn = new TableColumn("Id Kierowcy");
		driverIdColumn.setMinWidth(80);
		driverIdColumn.setMaxWidth(80);
		driverIdColumn.setCellValueFactory(new PropertyValueFactory<>("idKierowcy"));// zmienic

		// Smoke column
		TableColumn<PrzejazdWyszukiwanie, String> smokeColumn = new TableColumn("Palacy");
		smokeColumn.setMinWidth(70);
		smokeColumn.setMaxWidth(70);
		smokeColumn.setCellValueFactory(new PropertyValueFactory<>("palacy"));// zmienic

		// Gender column
		TableColumn<PrzejazdWyszukiwanie, String> genderColumn = new TableColumn("Preferowana plec");
		genderColumn.setMinWidth(120);
		genderColumn.setMaxWidth(120);
		genderColumn.setCellValueFactory(new PropertyValueFactory<>("plec"));// zmienic

		// Animal column
		TableColumn<PrzejazdWyszukiwanie, String> animalColumn = new TableColumn("Przewoz zwierzat");
		animalColumn.setMinWidth(120);
		animalColumn.setMaxWidth(120);
		animalColumn.setCellValueFactory(new PropertyValueFactory<>("zwierze"));// zmienic

		// Money column
		TableColumn<PrzejazdWyszukiwanie, String> moneyColumn = new TableColumn("Koszt [zl]");
		moneyColumn.setMinWidth(70);
		moneyColumn.setCellValueFactory(new PropertyValueFactory<>("koszt"));// zmienic
*/
		table = new TableView<>();
		table.getColumns().addAll(idColumn/*, fromColumn, nationFromColumn, toColumn, nationToColumn, dateColumn,
				timeColumn, spaceColumn, smokeColumn, genderColumn, animalColumn, moneyColumn, driverIdColumn*/);

		// Dodawanie wyszukanych elementow do tabeli
		table.setItems(getProducts());
		table.setColumnResizePolicy(table.CONSTRAINED_RESIZE_POLICY);

		return table;
	}
	
	
	
	// Wrzucenie wynikow wyszukiwania do listy
		public ObservableList<String> getProducts() {
			/*String tmp[] = przejazdy.split(" ");
			int i = 0;
			int iloscElementow = tmp.length;

			
			 * System.out.println("\n\nLiczba rekordow = " + (iloscElementow / 13) +
			 * "\n\n"); //jeden rekord sklada sie z 13 kolumn for(i=0;
			 * i<iloscRekordow; i++){ System.out.println(tmp[i]); } i = 0;
			 

			ObservableList<PrzejazdWyszukiwanie> travels = FXCollections.observableArrayList();
			while (i < iloscElementow)
				travels.add(new PrzejazdWyszukiwanie(tmp[i++], tmp[i++], tmp[i++], tmp[i++], tmp[i++], tmp[i++], tmp[i++],
						tmp[i++], tmp[i++], tmp[i++], tmp[i++], tmp[i++], tmp[i++]));

			return travels;*/
			
//			int i = 0;
			ObservableList<String> products = FXCollections.observableArrayList();
			products.add("asd");
			products.add("eee");
			System.out.println(products.get(0));
			return products;
		}
		
		public void wyswietl(String nazwaa){
			try {
				s = new Socket(InetAddress.getByName(nazwa), port);
				output = new DataOutputStream(s.getOutputStream());
				input = new DataInputStream(s.getInputStream());
				
				String polecenie = "15 " + nazwaa;
				
				try {
					System.out.println("Wysylam polecenie wyszukaj() na serwer");
					output.writeUTF(polecenie);
				} catch (IOException e1) {
					System.out.println("\n>> Blad wysylania polecenia na serwer <<\n");
					e1.printStackTrace();
				}

				System.out.println("Proba odbioru od serwera");
				try {
					String konto = input.readUTF();

					System.out.println("Wyszukano produkty():\n" + konto);
					tablica = konto.split(" ");
					
					

				} catch (IOException e2) {
					System.out.println("\n>> Blad odbioru od serwera <<\n");
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

}