package database.server;

import java.io.DataOutputStream;
import java.sql.CallableStatement;
import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;

import clientServerConnection.Server;

public class Database {

	// Connection Database
	public static Connection connectToDatabase() {
		String dataBaseUrl = "jdbc:oracle:thin:@localhost:1521:orcl";// orcl
		String userDataBase = "sony";
		String passDataBase = "admin321";
		Connection myConnection = null;
		try {
			// Connection Database
			myConnection = DriverManager.getConnection(dataBaseUrl, userDataBase, passDataBase);

		} catch (SQLException e) {
			System.out.println("Blad przy polaczeniu z baza danych!");
			System.exit(1);
		}
		return myConnection;
	}

	// ============================================================
	// Create Statment
	// tworzenie obiektu Statement przesy�aj�cego zapytania do bazy connection
	public static Statement createStatement(Connection connection) {
		try {
			return connection.createStatement();
		} catch (SQLException e) {
			System.out.println("B�ad createStatement!" + e.getMessage() + ": " + e.getErrorCode());
			System.exit(3);
		}
		return null;
	}

	// ============================================================
	// Close Connection
	// Zamykanie po��czenia z baz� danych
	public static void closeConnection(Connection connection, Statement statement) {
		System.out.println("\nZamykanie polaczenia z baz�:");
		try {
			statement.close();
			connection.close();
		} catch (SQLException e) {
			System.out.println("Bl�d przy zamykaniu pol�czenia z baz�! " + e.getMessage() + ": " + e.getErrorCode());
			System.exit(4);
		}
		System.out.println("Zamkni�cie OK");
	}

	// ============================================================
	// Wykonanie kwerendy i przes�anie wynik�w do obiektu ResultSet
	public static ResultSet executeQuery(Statement statement, String sql) {
		try {
			return statement.executeQuery(sql);
		} catch (SQLException e) {
			System.out.println("Zapytanie nie wykonane! " + e.getMessage() + ": " + e.getErrorCode());
		}
		return null;
	}

	// ============================================================
	public static int executeUpdate(Statement statement, String sql) {
		try {
			return statement.executeUpdate(sql);
		} catch (SQLException e) {
			System.out.println("Zapytanie nie wykonane! " + e.getMessage() + ": " + e.getErrorCode());
		}
		return -1;
	}

	// ============================================================
	// Wy�wietla dane uzyskane zapytaniem select
	public static String printDataFromQuery(ResultSet resultSet) {
		ResultSetMetaData resultSetMetData;
		String str = "";
		try {
			resultSetMetData = resultSet.getMetaData();
			int numcols = resultSetMetData.getColumnCount(); // pobieranie
																// liczby kolumn
			// wyswietlanie nazw kolumn:
			for (int i = 1; i <= numcols; i++) {
				System.out.print("\t" + resultSetMetData.getColumnLabel(i) + "\t|");
			}
			System.out.print("\n____________________________________________________________________________\n");
			/**
			 * r.next() - przej�cie do kolejnego rekordu (wiersza) otrzymanych
			 * wynik�w
			 */
			// wyswietlanie kolejnych rekordow:
			while (resultSet.next()) {
				for (int i = 1; i <= numcols; i++) {
					Object obj = resultSet.getObject(i);
					if (obj != null) {
						System.out.print("\t" + obj.toString() + "\t|");
						str += obj.toString() + " ";
					} else
						System.out.print("\t");
				}
				System.out.println();
			}
		} catch (SQLException e) {
			System.out.println("Bl�d odczytu z bazy! " + e.getMessage() + ": " + e.getErrorCode());
		}
		return str;
	}

	// ============================================================
	// Metoda pobiera dane na podstawie nazwy kolumny
	public static void sqlGetDataByName(ResultSet resultSet) {
		System.out.println("Pobieranie danych z wykorzystaniem nazw kolumn");
		try {
			ResultSetMetaData resultSetMetData = resultSet.getMetaData();
			int numcols = resultSetMetData.getColumnCount();
			// Tytul tabeli z etykietami kolumn zestawow wynikow
			for (int i = 1; i <= numcols; i++) {
				System.out.print(resultSetMetData.getColumnLabel(i) + "\t|\t");
			}
			System.out.print("\n____________________________________________________________________________\n");
			while (resultSet.next()) {
				int size = resultSet.getMetaData().getColumnCount();
				for (int i = 1; i <= size; i++) {
					switch (resultSet.getMetaData().getColumnTypeName(i)) {
					case "INT":
						System.out.print(resultSet.getInt(resultSet.getMetaData().getColumnName(i)) + "\t|\t");
						break;
					case "DATE":
						System.out.print(resultSet.getDate(resultSet.getMetaData().getColumnName(i)) + "\t|\t");
						break;
					case "VARCHAR":
						System.out.print(resultSet.getString(resultSet.getMetaData().getColumnName(i)) + "\t|\t");
						break;
					default:
						System.out.print(resultSet.getMetaData().getColumnTypeName(i));
					}
				}
				System.out.println();
			}
		} catch (SQLException e) {
			System.out.println("Bl�d odczytu z bazy! " + e.getMessage() + ": " + e.getErrorCode());
		}
	}

	
	
	
	
	public String wykonaniePolecenia(Connection conn, String s1) { // ResultSet
		String[] tab = s1.split(" ");
		int i = 0;
		String s = null;
		int liczba; // liczba - numer polecenia SQL
		liczba = Integer.parseInt(tab[0]);

		switch (liczba) {

		case 1: {
			System.out.println("Logowanie()");
				s = "select * from konto where nick = '"+tab[1]+ "' and haslo = '"+tab[2] + "'" ;
			//s = "1";
			//return s;
			break;
		}
		

		case 7: {
			System.out.println("Moje konto()");
			s = "select * from konto where iduzytkownika=" + tab[1];
			break;
		}
		
		case 15:
			System.out.println("Wyszukaj produkty()");
			System.out.println(tab[1]);
			s="select * from "+tab[1];
			break;
		
		}

		
		Statement st = createStatement(conn);
		ResultSet rs = executeQuery(st, s);
		
		System.out.println("\n\n");
		String str="0";
		str = printDataFromQuery(rs); 
		System.out.println("\n\n");
		

		//String str = "wykonaniePolecenia() zakonczone";
		return str;
	}
	
	
/*
 * TUTAJ SA TYLKO PROCEDURY - RACZEJ NIE BEDZIE NAM TO POTRZEBNE
 * 
 */
	public int wykonaniePolecenia2(Connection conn, String s1) {
		int id_rekordu = 0;
		int id_uzytkownika = 0;

		System.out.println("Aktualne idUzytkownika: " + Server.loggedUserId);

		CallableStatement cstmt;

		String[] tab = s1.split(" ");
		int i = 0;
		String s = null;
		int liczba; // liczba - numer polecenia SQL
		liczba = Integer.parseInt(tab[0]);

		try {
			switch (liczba) {
			// logowanie
			case 1: {
				System.out.println("Logowanie()");
				s = "{ call logowanie(?,?,?)}";

				cstmt = conn.prepareCall(s);
				for (i = 1; i < tab.length; i++)
					cstmt.setString(i, tab[i]);
				cstmt.registerOutParameter(tab.length, Types.INTEGER);
				cstmt.executeUpdate();
				id_uzytkownika = cstmt.getInt(tab.length);

				Server.loggedUserId = id_uzytkownika;
				System.out.println("Id zalogowanego uzytkownika: " + Server.loggedUserId);

				return id_uzytkownika;
			}
			// rejestracja
			case 2: {
				System.out.println("Rejestracja()");
				s = "{ call rejestracja(?,?,?,?,?)}";

				cstmt = conn.prepareCall(s);
				for (i = 1; i < tab.length; i++)
					cstmt.setString(i, tab[i]);
				cstmt.registerOutParameter(tab.length, Types.INTEGER);
				cstmt.executeUpdate();
				id_rekordu = cstmt.getInt(tab.length);

				// jesli 0 - zalogowano, jesli 1 - istnieje takie konto
				return id_rekordu;
			}
			// dodawanie przejazdu
			case 4: {
				System.out.println("Dodaj Przejazd()");
				s = "{call dodaj_przejazd(?,?,?,?,?,?,?,?,?,?,?,?)}";

				cstmt = conn.prepareCall(s);
				for (i = 1; i < tab.length; i++)
					cstmt.setString(i, tab[i]);
				cstmt.registerOutParameter(tab.length, Types.INTEGER);
				cstmt.executeUpdate();
				id_rekordu = cstmt.getInt(tab.length);

				return id_rekordu;
			}
			//rezerwacja przejazdu
			case 8: {
				System.out.println("Rezerwuj()");
				s = "{call zarezerwoj_przejazd(?,?,?)}";		//do zmiany w bazie danych na Zarezerwuj
				
				cstmt = conn.prepareCall(s);
				for (i = 1; i < tab.length; i++)
					cstmt.setString(i, tab[i]);
				cstmt.registerOutParameter(tab.length, Types.INTEGER);
				cstmt.executeUpdate();
				id_rekordu = cstmt.getInt(tab.length);

				System.out.println("Wolnych miejsc = " + id_rekordu);
				return id_rekordu;
			}
			//usuwanie przejazdu
			case 9:{
				System.out.println("Usun_przejazd()");
				s="{call usun_przejazd(?,?,?)}";
				cstmt = conn.prepareCall(s);
				for (i = 1; i < tab.length; i++)
					cstmt.setString(i, tab[i]);
				cstmt.registerOutParameter(tab.length, Types.INTEGER);
				cstmt.executeUpdate();
				id_rekordu = cstmt.getInt(tab.length);

				System.out.println("Id = " + id_rekordu);
				return id_rekordu;
			}
			case 10:{
				System.out.println("Usun_rezerwacje()");
				s="{call usun_rezerwacje(?,?,?)}";
				cstmt = conn.prepareCall(s);
				for (i = 1; i < tab.length; i++)
					cstmt.setString(i, tab[i]);
				cstmt.registerOutParameter(tab.length, Types.INTEGER);
				cstmt.executeUpdate();
				id_rekordu = cstmt.getInt(tab.length);

				System.out.println("Id = " + id_rekordu);
				return id_rekordu;
			}
			case 11:{
				System.out.println("edytuj()");
				s="{call edytuj(?,?,?,?,?,?,?,?,?,?)}";
				cstmt = conn.prepareCall(s);
				for (i = 1; i < tab.length; i++)
					cstmt.setString(i, tab[i]);
				cstmt.registerOutParameter(tab.length, Types.INTEGER);
				cstmt.executeUpdate();
				id_rekordu = cstmt.getInt(tab.length);

				System.out.println("Id = " + id_rekordu);
				return id_rekordu;
			}
			
			
			
			}
			
			cstmt = conn.prepareCall(s);
			for (i = 1; i < tab.length; i++)
				cstmt.setString(i, tab[i]);
			cstmt.registerOutParameter(tab.length, Types.INTEGER);
			cstmt.executeUpdate();
			id_rekordu = cstmt.getInt(tab.length);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return id_rekordu;
	}

}