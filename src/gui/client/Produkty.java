package gui.client;

import Safety.Asking;
import clientServerConnection.Server;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Produkty {
	private Stage stage;
	private Scene sceneLogOut;
	private Scene scene;

	private Button addButton;
	private Button searchButton;
	private Button reservButton;
	private Button accountButton;
	private Button travelButton;
	private Button chatButton;
	private Button logOutButton;
	private Button settingsButton;
	private Button backButton;
	//private RadioButton radioButton;
	private Label menuLabel;
	int userId;

	public Produkty(Stage stage, Scene sceneWyloguj, Scene scene, int userId) {
		this.stage = stage;
		this.stage.setTitle("Produkty");
		this.stage.setMinWidth(400);
		this.stage.setMinHeight(500);
		this.stage.setMaxWidth(400);
		this.stage.setMaxHeight(500);
		this.sceneLogOut = sceneWyloguj;
		this.userId = userId;
		initButtons();
		initlayouts();
	}

	private void initlayouts() {
		GridPane grid = new GridPane();
		grid.setPadding(new Insets(10, 10, 10, 10));// A set of inside offsets
													// for the 4 side of a
													// rectangular area
		grid.setVgap(8);
		grid.setHgap(10);
		// grid.setAlignment(Pos.CENTER);

		grid.getChildren().addAll(addButton, searchButton, reservButton, travelButton,settingsButton, accountButton,backButton, logOutButton);
		grid.setAlignment(Pos.CENTER);
		scene = new Scene(grid, 400, 400);
		stage.setScene(scene);
		stage.setTitle("Produkty");

		stage.show();
	}

	// ============================================================
	private void initButtons() {
		// ============================================================
		// Add travel Button
		addButton = new Button("Konsole");
		addButton.setPrefSize(150, 50);
		GridPane.setConstraints(addButton, 0, 1);
		addButton.setOnAction(e -> {
			System.out.println("Klikam wyswietl Konsole");
			new WyswietlProdukt(stage, sceneLogOut, scene, userId, false,"konsola");

			
			// new Produkty(stage,sceneLogOut,scene,userId);
			// travel = new Dodaj_przejazd(stage, sceneWyloguj, scene, userId);
		});
		// ============================================================
		// Search travel Button
		searchButton = new Button("Urz�dzenia mobilne");
		searchButton.setPrefSize(150, 50);
		GridPane.setConstraints(searchButton, 0, 2);
		searchButton.setOnAction(e -> {
			System.out.println("Klikam wy�wietl urz�dzenia mobilne");

			new WyswietlProdukt(stage, sceneLogOut, scene, userId,true,"telefon tablet");
			// search = new Wyszukaj_przejazd(stage, sceneWyloguj, scene,

			//new WyswietlProdukt(stage, sceneLogOut, scene, userId, false);

			// userId);
		});
		// ============================================================
		// Reservation Button
		reservButton = new Button("Telewizory i Kina domowe");
		reservButton.setPrefSize(150, 50);
		GridPane.setConstraints(reservButton, 0, 3);
		reservButton.setOnAction(e -> {
			System.out.println("Klikam wy�wietl telewizory i kina domowe");
			new WyswietlProdukt(stage, sceneLogOut, scene, userId,true,"telewizor kinodomowe");
			// reserve = new Moje_Rezerwacje(stage, sceneWyloguj, scene,
			// userId);
		});
		// ============================================================
		// Travels Button
		travelButton = new Button("Sprz�t Audio");
		travelButton.setPrefSize(150, 50);
		GridPane.setConstraints(travelButton, 0, 4);
		travelButton.setOnAction(e -> {
			System.out.println("Klikam wyswietl sprz�t audio");
			new WyswietlProdukt(stage, sceneLogOut, scene, userId,true,"sluchawki odbiornikisamochodowe");
			// myTravel =new Moje_przejazdy(stage, sceneWyloguj, scene, userId);
		});

		// Settings button
		settingsButton = new Button("Aparaty i sprz�t wideo");
		settingsButton.setPrefSize(150, 50);
		GridPane.setConstraints(settingsButton, 0, 5);
		settingsButton.setOnAction(e -> {
			System.out.println("Klikam wy�wietl aparaty i sprz�t wideo");
			new WyswietlProdukt(stage, sceneLogOut, scene, userId,true,"aparatforograficzny kamerawideo");
			// myTravel =new Moje_przejazdy(stage, sceneWyloguj, scene, userId);
		});

		// ============================================================
		// Account Button
		accountButton = new Button("Projektory");
		accountButton.setPrefSize(150, 50);
		GridPane.setConstraints(accountButton, 0, 6);
		accountButton.setOnAction(e -> {
			System.out.println("Klikam wyswietl projektory");
			new WyswietlProdukt(stage, sceneLogOut, scene, userId,false,"projektor");
			// new Moje_konto(stage, sceneLogOut, scene, userId);
			// account = new MyAccount(stage,sceneLogOut,scene,
			// Server.loggedUserId);
		});

		// Back Button
		backButton = new Button("Cofnij");
		backButton.setPrefSize(150, 50);
		GridPane.setConstraints(backButton, 0, 7);
		backButton.setOnAction(e -> {
			boolean result = Asking.display("Powr�t", "Czy na pewno chcesz sie cofn��?");
			if (result == true) {
				new MainMenu(stage, sceneLogOut, userId);
			}
		});

		// Log Out Button
		logOutButton = new Button("Wyloguj");
		logOutButton.setPrefSize(150, 50);
		GridPane.setConstraints(logOutButton, 0, 7);
		logOutButton.setOnAction(e -> {
			boolean result = Asking.display("Wylogowywanie", "Czy na pewno chcesz sie wylogowac?");
			if (result == true) {
				Server.loggedUserId = 0; // to nie dziala
				System.out.println("\nWylogowano\n");
				stage.setScene(sceneLogOut);
			}
		});

	}
	
	private void initButtons_2() {

		// Chat Button
		chatButton = new Button("Otworz Czat");
		chatButton.setPrefSize(150, 50);
		GridPane.setConstraints(chatButton, 0, 1);
		chatButton.setOnAction(e -> {
			// chat = new Czat(stage, sceneLogout, sceneBack, Klient.id);
		});

		// Add travel Button
		addButton = new Button("Dodaj Przejazd");
		addButton.setPrefSize(150, 50);
		GridPane.setConstraints(addButton, 0, 2);
		addButton.setOnAction(e -> {
			//travel = new Dodaj_przejazd(stage, sceneLogout, sceneBack, idUzytkownika);
		});

		// Search travel Button
		searchButton = new Button("Wyszukaj Przejazd");
		searchButton.setPrefSize(150, 50);
		GridPane.setConstraints(searchButton, 0, 3);
		// searchButton.setOnAction(e -> {

		// A PO CO TO? NA TYM EKRANIE WYSZUKAJ PRZEJAZD W MENU W OGOLE NIE
		// POWINNO BYC SWOJA DROGA, TAKIE MOJE ZDANIE - z�oty

		// search = new Wyszukaj_przejazd(stage, sceneLogout, sceneBack,
		// Klient.id);
		// System.out.println("search.wyszukajPrzejazd() - Wyszukaj
		// przejazd.wyszukaj przejazd" );
		// search.wyszukajPrzejazd();
		// });

		// Reservation Button
		reservButton = new Button("Moje Rezerwacje");
		reservButton.setPrefSize(150, 50);
		GridPane.setConstraints(reservButton, 0, 4);
		// reservButton.setOnAction(e -> reserve = new Moje_Rezerwacje(stage,
		// sceneLogout, sceneBack, Klient.id));

		// Travels Button
		travelButton = new Button("Moje Przejazdy");
		travelButton.setPrefSize(150, 50);
		GridPane.setConstraints(travelButton, 0, 5);
		// travelButton.setOnAction(e -> myTravel = new Moje_przejazdy(stage,
		// sceneLogout, sceneBack, Klient.id));

		// Account Button
		accountButton = new Button("Moje Konto");
		accountButton.setPrefSize(150, 50);
		GridPane.setConstraints(accountButton, 0, 6);
		// accountButton.setOnAction(e -> account = new Moje_konto(stage,
		// sceneLogout, sceneBack, Klient.id));

		// Log Out Button
		logOutButton = new Button("Wyloguj");
		logOutButton.setPrefSize(150, 50);
		GridPane.setConstraints(logOutButton, 0, 8);
		logOutButton.setOnAction(e -> {
			boolean result = Asking.display("Wylogowywanie", "Czy na pewno chcesz sie wylogowac?");
			if (result == true) {
				//Server.idZalogowanego = 0; // to nie dziala
				//System.out.println("\nWylogowano\n");
				stage.setScene(sceneLogOut);
			}//
		});

		// Back Button
		backButton = new Button("Cofnij");
		backButton.setPrefSize(150, 50);
		GridPane.setConstraints(backButton, 0, 7);
		backButton.setOnAction(e -> {
			boolean result = Asking.display("Powr�t", "Czy na pewno chcesz sie cofn��?");
			if (result == true) {
				stage.setScene(scene);
				// glowneMenu = new GlowneMenu(stage, sceneWyloguj,
				// idUzytkownika);
			}
		});
	}

	// ============================================================
	/*private void initLabel_2() {

		// Menu Label
		menuLabel = new Label("Menu");
		menuLabel.setPrefSize(150, 50);
		GridPane.setConstraints(menuLabel, 0, 0);
	}

	// ============================================================
	public GridPane createGridMenu() {
		GridPane grid = new GridPane();
		grid.setPadding(new Insets(10, 10, 10, 10));
		grid.setVgap(10);
		grid.setHgap(10);
		grid.setAlignment(Pos.CENTER_LEFT);
		// ============================================================
		initButtons_2();
		initLabel_2();
		// ============================================================
		grid.getChildren().addAll(menuLabel, chatButton, addButton, searchButton, reservButton, travelButton,
				accountButton, logOutButton, backButton);
		return grid;
	}*/
	
}