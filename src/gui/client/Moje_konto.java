package gui.client;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

import Safety.Asking;
//import Safety.Modify;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;


public class Moje_konto {
	
	private Stage stage;
	private Scene sceneBack;// scena do powrotu
	private Scene sceneLogout;
	
	private Button editButton;
	private Button saveButton;
	private Button cancelButton;
	
	private Button chatButton;
	private Button addButton;
	private Button searchButton;
	private Button reservButton;
	private Button travelButton;
	private Button accountButton;
	private Button logOutButton;
	private Button backButton;
	
	private Label menuLabel;


	private Label nickLabel;
	private Label nameLabel;
	private Label surnameLabel;
	private Label mailLabel;
	private Label maleLabel;
	private Label passLabel;
	private Label yearLabel;
	private Label numberLabel;
	
	private Label editLabel;
	private Label saveLabel;
	private Label cancelLabel;
	
	//private Czat chat;
	//private Dodaj_przejazd travel;
//	private Wyszukaj_przejazd search;
	//private Moje_konto account;
	//private Czat czat;
	//private Moje_Rezerwacje reserve;
	//private Moje_przejazdy myTravel;
//	private GlowneMenu glowneMenu;
//	private Modify edycja;
	int id;
	
	
	public DataInputStream input;
	public DataOutputStream output;
	Socket s;
	int port = 1234;
	String nazwa = "127.0.0.1";
	String konto = "";
	String[] tablica;
	
	public Moje_konto(Stage stage, Scene sceneWyloguj,Scene sceneBack,int id){
		this.stage = stage;
		this.stage.setMinWidth(650);
		this.stage.setMinHeight(600);
		this.stage.setMaxWidth(650);
		this.stage.setMaxHeight(600);
		this.sceneLogout = sceneWyloguj;
		this.sceneBack = sceneBack;
		this.id = id;
		
		mojeKonto();
		initLayouts();
		
		//pobierzDaneUzytkownika();
	}
//============================================================
	private void initLayouts() {
		GridPane gridMenu = new GridPane();
		GridPane gridAccount = new GridPane();
		GridPane gridButton = new GridPane();
		gridMenu = createGridMenu();
		gridAccount = createGridAccount();
		gridButton = createGridButton();

		BorderPane borderPane = new BorderPane();
		borderPane.setLeft(gridMenu);
		borderPane.setCenter(gridAccount);
		borderPane.setRight(gridButton);

		Scene scene = new Scene(borderPane, 600, 400);
		stage.setScene(scene);
		stage.setTitle("Moje Konto");
		stage.show();

	}

// ============================================================
	public GridPane createGridButton() {
		GridPane grid = new GridPane();
		grid.setPadding(new Insets(10, 10, 10, 10));
		grid.setVgap(10);
		grid.setHgap(10);
		grid.setAlignment(Pos.CENTER_RIGHT);
	// ============================================================
		initLabel_3();
		initButtons_3();
	// ============================================================
		grid.getChildren().addAll(editLabel,editButton,saveButton,cancelButton);
		return grid;
	}
	//============================================================
		public void initLabel_3(){
		// Edit Label
		editLabel = new Label("Edytuj Dane");
		GridPane.setConstraints(editLabel, 0, 4);

//		// Save Label
//		saveLabel = new Label("Zapisz");
//		GridPane.setConstraints(saveLabel, 0, 6);
//
//		// Cancel Label
//		cancelLabel = new Label("Anuluj");
//		GridPane.setConstraints(cancelLabel, 0, 8);
						
			
		}
	//============================================================
		private void initButtons_3() {
			// Edit Button
			editButton = new Button("Edytuj");
			editButton.setPrefSize(100, 50);
			GridPane.setConstraints(editButton, 0, 5);
			editButton.setOnAction(e -> {
				//edycja =Modify.display(id, "Edycja", "password");
				//edycja = new Modify();
				//edycja.display(id, "Edycja", "password");
			});
			
			// Save Button
			saveButton = new Button("Zapisz");
			saveButton.setPrefSize(100, 50);
			GridPane.setConstraints(saveButton, 0, 6);
			saveButton.setOnAction(e -> {
				// Modify.display(user, "Edycja", "password");
				mojeKonto();
				
			});
					
			// Cancel Button
			cancelButton = new Button("Anuluj");
			cancelButton.setPrefSize(100, 50);
			GridPane.setConstraints(cancelButton, 0, 7);
			cancelButton.setOnAction(e -> {
				// Modify.display(user, "Edycja", "password");
			});
		}
		
	
// ============================================================
	private GridPane createGridAccount() {
		GridPane grid = new GridPane();
		grid.setPadding(new Insets(10, 10, 10, 10));
		grid.setVgap(10);
		grid.setHgap(10);
		grid.setAlignment(Pos.CENTER);

	// ============================================================
		
		initLabel();
	// ============================================================
		grid.getChildren().addAll(nickLabel,nameLabel,surnameLabel/*,mailLabel,maleLabel*/,passLabel/*,yearLabel,numberLabel*/);
		
		return grid;
	}
// ============================================================
	private void initLabel() {
	
		// Nick	Label
		nickLabel = new Label("Nick:  " + tablica[3]);
		nickLabel.setPrefSize(150, 50);
		GridPane.setConstraints(nickLabel, 0,1);
		
		// Name Label
		nameLabel = new Label("Imie: "+ tablica[1]);
		nameLabel.setPrefSize(150, 50);
		GridPane.setConstraints(nameLabel, 0, 2);
				
		// Surname Label
		surnameLabel = new Label("Nazwisko: " + tablica[2]);
		surnameLabel.setPrefSize(150, 50);
		GridPane.setConstraints(surnameLabel, 0, 3);
				
		// Mail Label
		//mailLabel = new Label("E-mail: "/*+ tablica[5]*/);
		//mailLabel.setPrefSize(150, 50);
		//GridPane.setConstraints(mailLabel, 0, 4);
				
		// Male Label
		//maleLabel = new Label("P�e�: "/*+ tablica[6]*/);
		///maleLabel.setPrefSize(150, 50);
		//GridPane.setConstraints(maleLabel, 0, 5);
				
		// Pass Label
		passLabel = new Label("Haslo: " + tablica[4]);
		passLabel.setPrefSize(150, 50);
		GridPane.setConstraints(passLabel, 0, 6);
				
		// Year Label
		//yearLabel = new Label("Rok Urodzenia: "/*+ tablica[7]*/);
		//yearLabel.setPrefSize(150, 50);
		//GridPane.setConstraints(yearLabel, 0, 7);
				
		// Number Label
		//numberLabel = new Label("Nr.Telefonu: "/* + tablica[8]*/);
		//numberLabel.setPrefSize(150, 50);
		//GridPane.setConstraints(numberLabel, 0, 8);
				
		
	}

// ============================================================
	private void initButtons_2() {

		// Chat Button
		//chatButton = new Button("Otworz Czat");
		//chatButton.setPrefSize(150, 50);
		//GridPane.setConstraints(chatButton, 0, 1);
		//chatButton.setOnAction(e -> {
			//chat = new Czat(stage, sceneLogout, sceneBack, Klient.id);
		//});

		// Add travel Button
		addButton = new Button("Dodaj Przejazd");
		addButton.setPrefSize(150, 50);
		GridPane.setConstraints(addButton, 0, 2);
		addButton.setOnAction(e -> {
			//travel = new Dodaj_przejazd(stage, sceneLogout, sceneBack, Klient.id);
		});

		// Search travel Button
		searchButton = new Button("Wyszukaj Przejazd");
		searchButton.setPrefSize(150, 50);
		GridPane.setConstraints(searchButton, 0, 3);
		searchButton.setOnAction(e -> {
			//search = new Wyszukaj_przejazd(stage, sceneLogout, sceneBack, Klient.id);
		});

		// Reservation Button
		reservButton = new Button("Moje Rezerwacje");
		reservButton.setPrefSize(150, 50);
		GridPane.setConstraints(reservButton, 0, 4);
		//reservButton.setOnAction(e -> reserve = new Moje_Rezerwacje(stage, sceneLogout, sceneBack, Klient.id));

		// Travels Button
		travelButton = new Button("Moje Przejazdy");
		travelButton.setPrefSize(150, 50);
		GridPane.setConstraints(travelButton, 0, 5);
		//travelButton.setOnAction(e -> myTravel = new Moje_przejazdy(stage, sceneLogout, sceneBack, Klient.id));

		// Account Button
		accountButton = new Button("Moje Konto");
		accountButton.setPrefSize(150, 50);
		GridPane.setConstraints(accountButton, 0, 6);
		//accountButton.setOnAction(e -> account = new Moje_konto(stage, sceneLogout, sceneBack, Klient.id));

		// Log Out Button
		logOutButton = new Button("Wyloguj");
		logOutButton.setPrefSize(150, 50);
		GridPane.setConstraints(logOutButton, 0, 8);
		logOutButton.setOnAction(e -> {
			boolean result = Asking.display("Wylogowywanie", "Czy na pewno chcesz sie wylogowac?");
			if (result == true) {
				stage.setScene(sceneLogout);
			}
		});

		// Back Button
		backButton = new Button("Cofnij");
		backButton.setPrefSize(150, 50);
		GridPane.setConstraints(backButton, 0, 7);
		backButton.setOnAction(e -> {
			boolean result = Asking.display("Powr�t", "Czy na pewno chcesz sie cofn��?");
			if (result == true) {
				  new MainMenu(stage,sceneLogout,id);
			}
		});
	}
// ============================================================
	private void initLabel_2() {

		// Menu Label
		menuLabel = new Label("Menu");
		menuLabel.setPrefSize(150, 50);
		GridPane.setConstraints(menuLabel, 0, 0);
	}
// ============================================================
	public GridPane createGridMenu() {
		GridPane grid = new GridPane();
		grid.setPadding(new Insets(10, 10, 10, 10));
		grid.setVgap(10);
		grid.setHgap(10);
		grid.setAlignment(Pos.CENTER);
	// ============================================================
		initButtons_2();
		initLabel_2();
	// ============================================================
		grid.getChildren().addAll(menuLabel/*,chatButton,addButton,searchButton,reservButton,travelButton*/,accountButton,logOutButton,backButton);
		return grid;
	}
	
	public void mojeKonto(){
		
			try {
				s = new Socket(InetAddress.getByName(nazwa), port);
				output = new DataOutputStream(s.getOutputStream());
				input = new DataInputStream(s.getInputStream());

				String polecenie = "7 " + id;
				try {
					System.out.println("Wysylam polecenie Moje konto() na serwer");
					output.writeUTF(polecenie);
				} catch (IOException e1) {
					System.out.println("\n>> Blad wysylania polecenia na serwer <<\n");
					e1.printStackTrace();
				}

				System.out.println("Proba odbioru od serwera");
				try {
					konto = input.readUTF();

					System.out.println("Wyszukano Moje konto():\n" + konto);

					// Dodawanie przejazdow do grida
					//gridTravel.getChildren().addAll(createGridTable());
					
					tablica = konto.split(" ");
					
					

				} catch (IOException e2) {
					System.out.println("\n>> Blad odbioru od serwera <<\n");
				}

			} catch (IOException e3) {
				// TODO Auto-generated catch block
				System.out.println("IOException e3 - Dodaj przejazd");
				e3.printStackTrace();
			}
	

		
		
	/*	id=Klient.id;
		account = new Moje_konto(stage, sceneLogout, sceneBack, id);
		System.out.println("Id w Moje_konto.mojeKonto: "+id);
		String str="7 " + id;
		System.out.println("String mojeKonto:" +str);
		try {
			Klient.dout.writeUTF(str);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		String s="";
		s=Klient.str;
		System.out.println("Moje_konto.mojeKonto() 1: klient.str " +s);
		
		try {
			s=Klient.din.readUTF();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Moje_konto.mojeKonto() 2: klient.str " +s);
		
	}
	
	
	
	private void pobierzDaneUzytkownika(){
		String s1;
		s1 = "select imie from Uzytkownik where idUzytkownika = " + Klient.id;
		
	}
*/
}
}