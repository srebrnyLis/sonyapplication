package clientServerConnection;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

public class FutureTaskCallback<T> extends FutureTask<T> {

	public FutureTaskCallback(Callable<T> callable) {
		super(callable);
	}
	
	
	
	public void done() {
		String msg = "Wynik: ";
		if (isCancelled())
			msg += "Anulowane.";
		else {
			try {
				msg += get();
			} catch (Exception exc) {
				msg += exc.toString();
			}
		}
		System.out.println("\n" + msg + "\n");
	}

}